
**NOTES:**

`original_textures.zip` contains the textures that can be used to generate VTFF page files for the demo
applications in the "demos/" base dir. Pre-generated `.vt` page files are not included in this repository
because of the large size of such files. Therefore, to run the demos, you'll have to decompress
`original_textures.zip` and run the `vtmake` command-line tool for each texture to generate fresh page files.

